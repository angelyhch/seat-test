from tkinter import *
import time
from loggers import dev_logger, prod_logger
from funcbase import *


class FunctestR7C6(FunctestTemplate):
    def initData(self):
        MsgId = 0x280
        self.idIntVar.set(MsgId)
        self.testDatas = [
            UnitTestDataStructure(
                "发动机工作",
                "0000000000840000",
                "0000000000ff0000",
            ),
        ]

    def sendControlMsg(self, key):
        # wrapper包装是给按钮的回调函数用
        def wrapper():
            # 临时创建一个包装函数worker,把循环发送的任务放进去,然后再把这个函数丢进线程池中持续执行.
            def worker():
                while True:
                    # 循环发送间隔0.5s
                    time.sleep(0.1)
                    self._sendControlMsg(key)

            self.MASTER.pool.submit(worker)

        return wrapper

    def _sendControlMsg(self, key):
        """
        执行具体单个测试的函数闭包
        """

        strData = self.testDatas[key].send
        data = strData
        # dev_logger.debug(data)

        # 发送信息
        # with self.MASTER.recvMsgDequeLock:
            # self.MASTER.recvMsgDeque.clear()
        res = self.MASTER.sendcan1_manual(self.idIntVar.get(), data_string=data)
        if res == "error":
            self.sendDataLabelDict[key].configure(text="发送NG", background="red")
            return "error"
        else:
            self.sendDataLabelDict[key].configure(text="发送OK", background="green")
            # 正确发送后, 从总线读取响应信息
            # 读取之前把信息栈清空,然后再经过一段时间判断,收到的信息里有没有预期的数据
            # recvResult = self.checkRecvMsg()
            recvResult = True
            if recvResult is True:
                self.recvDataLabelDict[key].configure(text="返回OK", background="green")
            else:
                self.recvDataLabelDict[key].configure(text="返回NG", background="red")


class FunctestR7C7(FunctestTemplate):
    def initData(self):
        MsgId = 0x570
        self.idIntVar.set(MsgId)

        self.testDatas = [
            UnitTestDataStructure(
                "整车上电",
                "1000000080000000",
                "f0000000ff000000",
            ),
        ]

    def sendControlMsg(self, key):
        # 保存传递进来的key参数, wrapper包装是给按钮的回调函数用

        def wrapper():
            # 临时创建一个包装函数worker,把循环发送的任务放进去,然后再把这个函数丢进线程池中持续执行.
            dev_logger.debug("wrapper running....")

            def worker():
                dev_logger.debug("send while outer running....")
                while True:
                    # dev_logger.debug("send while inner running....")
                    # 循环发送间隔0.5s
                    time.sleep(0.1)
                    self._sendControlMsg(key)

            self.MASTER.pool.submit(worker)

        return wrapper

    def _sendControlMsg(self, key):
        """
        执行具体单个测试的函数闭包
        """

        strData = self.testDatas[key].send
        data = strData
        # dev_logger.debug(data)

        # 发送信息
        # dev_logger.debug(self.MASTER.recvMsgDeque)
        # with self.MASTER.recvMsgDequeLock:
            # self.MASTER.recvMsgDeque.clear()
        # dev_logger.debug(self.MASTER.recvMsgDeque)
        res = self.MASTER.sendcan1_manual(self.idIntVar.get(), data_string=data)
        if res == "error":
            self.sendDataLabelDict[key].configure(text="发送NG", background="red")
            return "error"
        else:
            self.sendDataLabelDict[key].configure(text="发送OK", background="green")
            # 正确发送后, 从总线读取响应信息
            # 读取之前把信息栈清空,然后再经过一段时间判断,收到的信息里有没有预期的数据
            # recvResult = self.checkRecvMsg()
            recvResult = True
            if recvResult is True:
                self.recvDataLabelDict[key].configure(text="返回OK", background="green")
            else:
                self.recvDataLabelDict[key].configure(text="返回NG", background="red")
